import 'package:flutter/material.dart';

void main(){
  runApp(MyApp());
}

//variable, first image discription
var childDescription = 'Acquarium 10-2018, 2 Years Old';
var buttonCounter = 0;

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(childDescription, style: TextStyle(
                    fontWeight: FontWeight.bold,),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
    Color color = Theme
        .of(context)
        .primaryColor;

    Column _buildButtonColumn(Color color, IconData icon, String label) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(icon, color: color),
          Container(
            margin: const EdgeInsets.only(top: 8),
            child: Text(
              label,
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: color,
              ),
            ),
          ),
        ],
      );
    }

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.arrow_back, 'Previous'),
          _buildButtonColumn(color, Icons.arrow_forward, 'Next'),
        ],
      ),
    );
    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'First visit to the La Jolla Acquarium in 2018.',
        softWrap: true,
      ),
    );
    return MaterialApp(
      title: 'Layout Homework',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Can I See Photos of Your Kid?'),
        ),
        body: ListView(
            children: [
              Image.asset(
                'images/1zoo.jpg',
                width: 600,
                height: 540,
                fit: BoxFit.cover,
              ),
              titleSection,
              buttonSection,
              textSection
            ]
        ),
      ),
    );
  }
}  //MyApp
